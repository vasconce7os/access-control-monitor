import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import 'rxjs/add/operator/map'
import 'rxjs/add/operator/catch';
import { Observable } from "rxjs/Observable";

@Injectable({
  providedIn: 'root'
})
export class DataService
{

  baseUrl = "http://localhost:80/api";

  constructor(private http: HttpClient) { }

  getHistories()
  {
    return this.http.get(this.baseUrl + '/histories');
  }

  getEmployee(id)
  {
    return this.http.get(this.baseUrl + '/histories/employee/' + id);
  }
}
