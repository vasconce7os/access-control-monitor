import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AccessComponent } from './access.component';

@NgModule({
  imports: [
    CommonModule,
  ],
  declarations: [AccessComponent]
})

export class ProductModule { }
