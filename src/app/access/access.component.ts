import { Component, OnInit } from '@angular/core';

import { DataService } from '../data.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-access',
  templateUrl: './access.component.html',
  styleUrls: ['./access.component.scss']
})
export class AccessComponent implements OnInit {

  histories$: Object;

  constructor(private data: DataService) { }

  ngOnInit() {
    this.data.getHistories().subscribe(
      data => this.histories$ = data
    );
  }

}
